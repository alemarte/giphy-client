# Giphy Client

## Api Endpoint

#### Search

`https://developers.giphy.com/docs/api/endpoint#search`

The search endpoint is used to search for images.

Queries are always performed with a `limit` parameter which is configured by default to _25_ and optionally a `q` parameter to query results by term or phrase.

When the application demands more results, following up queries are performed with an `offset` until the entire result set is retrieved.

Every time the query parameter changes, the offset is reset.

## Explanation

### Included Libraries:

**Angular Material** https://material.angular.io/

**Tailwind Css** https://tailwindcss.com/

### Routes

`/` 
The home page is the search page.

`/favorites`
Display the list of the favorites images.

### Shared Components
**MasonryComponent**

Displays items in columns. Items are equally distributed based on the overall computed height.

**ItemComponent**

Display the image with a lazy loading strategy. As soon as the component is visible in the viewport, the source of the img tag is populated.

During the process a randomic backgorund may be displayed.

**ItemDetailComponent**

This component is instantiated as MatDialog. It displays details of the image and allows the user to toggle favorites.

### Shared Services

**MediaWatcherService**

This service provides method to detect media changes which affect the masonry layout
(in particular the number of columns used to distribute the retrieved images).

**Giphy Service**

This service is responsible to keep the state of the current research (items, pagination) and provides method to ask for new fresh searches and more items.

Data is exposed by mean of Observables following a redux pattern.

**Giphy Favorite Service**

This service is responsible to manage the list of favourites images which are persisted in the Browser localStorage.

Data is exposed by mean of Observables following a redux pattern.

## Run the application

Before running the application, the `API_KEY` constant in the `app.module.ts` file must be populated with a valid Giphy api key.

Run `npm install`

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Missing or partially missing parts

- Tests are not included in this prototype.
- Sorting of images.
- Some Styles improvements

