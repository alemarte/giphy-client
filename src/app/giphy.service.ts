import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, take, throttleTime } from 'rxjs/operators';
import { GiphyImage, GiphyPagination, GiphyResponse } from './giphy.model';
import { API_KEY, API_SEARCH_ENDPOINT, SEARCH_DEBOUNCE, SEARCH_DEFAULT_LIMIT } from './app.module';



@Injectable({
  providedIn: 'root'
})
export class GiphyService {

  // Private
  private _images$: BehaviorSubject<GiphyImage[] | null> = new BehaviorSubject([]);
  private _pending$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _hasMore$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _query$: BehaviorSubject<string> = new BehaviorSubject(null);
  private _search$: BehaviorSubject<string> = new BehaviorSubject(null);

  private _pagination: GiphyPagination;
  private _limit: number =  SEARCH_DEFAULT_LIMIT;

  /**
   * Constructor
   */
  constructor(private _httpClient: HttpClient) {
    this._search$
      .pipe(debounceTime(SEARCH_DEBOUNCE), throttleTime(SEARCH_DEBOUNCE))
      .subscribe((q) => {

        if (!!q) {
          this._query$.next(q);
          this._pending$.next(true);
          this._searchImpl(q, this._limit, 0).pipe(take(1)).subscribe((res) => {
            this._processResponse(res, true);
          })
        } else {
          this.reset();
        }
    })
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Observe images.
   */
  images$(): Observable<GiphyImage[]> {
    return this._images$.asObservable();
  }

  /**
   * Observe pending state.
   */
  pending$(): Observable<boolean> {
    return this._pending$.asObservable();
  }

  /**
   * Observe the query param.
   */
  query$(): Observable<string> {
    return this._query$.asObservable();
  }

  /**
   * Perform a new Search.
   */
  search(q: string) {
    this._search$.next(q);
  }

  /**
   * Fetch more images for the latest Search.
   */
  more() {
    if (this._hasMore$.getValue() && !this._pending$.getValue()) {
      this._pending$.next(true);

      this._searchImpl(this._query$.getValue(), this._limit, this._pagination.offset + this._limit)
        .pipe(take(1))
        .subscribe((res) => {

          this._processResponse(res, false);

        })
    }
  }

  /**
   * Reset the query
   */
  reset()
  {
    this._query$.next(null);
    this._pending$.next(false);
    this._images$.next([]);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private _searchImpl(q: string, limit: number, offset: number): Observable<GiphyResponse> {

    let params = new HttpParams()
      .set('limit', limit)
      .set('offset', offset)
      .set('api_key', API_KEY);

    if (q) {
      params = params.set('q', q);
    }

    return this._httpClient.get<GiphyResponse>(API_SEARCH_ENDPOINT, { params: params });
  }

  private _processResponse(response: GiphyResponse, reset: boolean) {

    this._images$
      .pipe(take(1))
      .subscribe((images) => {

        this._pagination = response.pagination;

        this._hasMore$.next(
          response.pagination.total_count - (response.pagination.count + response.pagination.offset) > 0
        );

        if (reset) {
          this._images$.next([...response.data]);
        } else {
          this._images$.next([...images, ...response.data]);
        }
        this._pending$.next(false);
      })

  }


}
