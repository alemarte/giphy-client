import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GiphyImage } from './giphy.model';

const LOCAL_STORAGE_KEY = 'FAVOURITES';

@Injectable({
  providedIn: 'root'
})
export class GiphyFavoriteService {

  private _favorites$: BehaviorSubject<GiphyImage[] | null>;

  /**
   * Constructor
   */
  constructor() {
    // load favorite
    const favorites = this._loadFavorites();
    this._favorites$ = new BehaviorSubject(favorites);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Observe the favourites.
   */
  favorites$(): Observable<GiphyImage[]> {
    return this._favorites$.asObservable();
  }


  /**
   * Observe whether an image is favorite or not.
   */
  isFavorites$(image:GiphyImage): Observable<boolean> {
    return this._favorites$.asObservable().pipe(
      map((favorites) => this._isFavorite(image))
    );
  }

  /**
   * Add an image to the favorites.
   */
  toggleFavorite(image: GiphyImage) {
    const isFavorite = this._isFavorite(image);
    if (isFavorite) {
      this._favorites$.next(this._removeFavorite(image));
    } else {
      this._favorites$.next(this._addFavorite(image));
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private _loadFavorites(): GiphyImage[] {
    let favorites;
    let value = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (!value) {
      favorites = [];
      localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(value));
    } else {
      favorites = JSON.parse(value);
    }
    return favorites;
  }

  private _addFavorite(image: GiphyImage): GiphyImage[] {

    const favorites = [...this._favorites$.getValue(), image];

    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(favorites));

    return favorites;

  }

  private _removeFavorite(image: GiphyImage): GiphyImage[] {

    const favorites = this._favorites$.getValue().filter(img => img.id !== image.id);

    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(favorites));

    return favorites;

  }

  private _isFavorite(image: GiphyImage): boolean {

    const favorites = this._favorites$.getValue();
    return favorites ? this._favorites$.getValue().find(img => img.id === image.id) !== undefined : false;

  }



}
