import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MediaWatcherService } from '../shared/services/media-watcher.service';
import { GiphyService } from '../giphy.service';
import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/overlay';
import { MasonryItem } from '../shared/component/masonry/masonry.component';
import { GiphyImage } from '../giphy.model';


@Component({
    selector       : 'app-home',
    templateUrl    : './home.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {

    items: MasonryItem<GiphyImage>[];
    pending: boolean = false;
    noResult: boolean = false;
    masonryColumns: number = 4;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _ref: ElementRef,
        private _changeDetectorRef: ChangeDetectorRef,
        private _MediaWatcherService: MediaWatcherService,
        private _scrollDispatcher: ScrollDispatcher,
        private _matDialog: MatDialog,
        private _giphyService: GiphyService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        // Subscribe to search service changes
        combineLatest([
          this._giphyService.images$(),
          this._giphyService.pending$(),
          this._giphyService.query$()
        ])
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(([images, pending, query]) => {
               this.items = images.map((image) => ({ data: image, weight: +image.images.fixed_width_downsampled.height}));
               this.pending = pending && !!query;
               this.noResult = !pending && !!query && images.length === 0;
               this._changeDetectorRef.detectChanges();
          });

        // Subscribe to media changes
        this._MediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({matchingAliases}) => {

                // Set the masonry columns
                //
                // This if block structured in a way so that only the
                // biggest matching alias will be used to set the column
                // count.
                if ( matchingAliases.includes('xl') )
                {
                    this.masonryColumns = 5;
                }
                else if ( matchingAliases.includes('lg') )
                {
                    this.masonryColumns = 4;
                }
                else if ( matchingAliases.includes('md') )
                {
                    this.masonryColumns = 3;
                }
                else if ( matchingAliases.includes('sm') )
                {
                    this.masonryColumns = 2;
                }
                else
                {
                    this.masonryColumns = 1;
                }

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to scroll event to fetch more
        this._scrollDispatcher.scrolled(100).subscribe((scrollable: CdkScrollable) => {
            if (scrollable.measureScrollOffset('bottom') < 10) {
                this._giphyService.more();
            }
        })
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();

        // Reset the search service
        this._giphyService.reset();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter by query
     *
     * @param query
     */
    filterByQuery(query): void {
       this._giphyService.search(query);
    }

    /**
     * Column track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFnColumn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Items track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFnItem(index, item: any): any {
        return item.data.id;
    }


}
