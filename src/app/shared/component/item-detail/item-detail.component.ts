import {
    ChangeDetectionStrategy,
    Component, Input,
    OnDestroy,
    OnInit,
    ViewEncapsulation
} from '@angular/core';
import { GiphyFavoriteService } from '../../../giphy-favorite.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GiphyImage } from '../../../giphy.model';


@Component({
    selector       : 'app-item-detail',
    templateUrl    : './item-detail.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemDetailComponent implements OnInit, OnDestroy {

    @Input() item: GiphyImage;

    @Input() favorite: boolean;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
      private _giphyFavoriteService: GiphyFavoriteService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit() {
        this._giphyFavoriteService.isFavorites$(this.item)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((favorite) => (this.favorite = favorite))
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    toggleFavorite() {
        this._giphyFavoriteService.toggleFavorite(this.item);
    }

}
