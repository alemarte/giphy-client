import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasonryComponent } from './masonry/masonry.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { ItemComponent } from './item/item.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [
        MasonryComponent,
        ItemComponent,
        ItemDetailComponent
    ],
    imports     : [
        CommonModule,

        MatDialogModule,
        MatIconModule,
        MatButtonModule
    ],
    exports     : [
        MasonryComponent,
        ItemComponent,
        ItemDetailComponent,
    ]
})
export class SharedModule
{
}
