import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component, ElementRef,
    Input,
    OnDestroy,
    OnInit, ViewChild,
    ViewEncapsulation
} from '@angular/core';
import { Subject } from 'rxjs';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { debounceTime, take, takeUntil } from 'rxjs/operators';
import { MediaWatcherService } from '../../services/media-watcher.service';
import { MatDialog } from '@angular/material/dialog';
import { ItemDetailComponent } from '../item-detail/item-detail.component';
import { GiphyImage } from '../../../giphy.model';
import {
    SKELETON_COLOR_PALETTE,
    VISIBILITY_CHECK_DEBOUNCE_ON_MEDIA_CHANGE,
    VISIBILITY_CHECK_THROTTLE_ON_SCROLL
} from '../../../app.module';

@Component({
    selector       : 'app-item',
    templateUrl    : './item.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('img') image;

    @Input() item: GiphyImage;

    actualHeight: number;

    skeletonColor: string;

    private _madeVisible: Subject<any> = new Subject<any>();

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _ref: ElementRef,
        private _changeDetectorRef: ChangeDetectorRef,
        private _scrollDispatcher: ScrollDispatcher,
        private _MediaWatcherService: MediaWatcherService,
        private _dialog: MatDialog
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.skeletonColor = this._selectRandomColor();

        // check the visibility when scroll events occur
        this._scrollDispatcher.ancestorScrolled(this._ref, VISIBILITY_CHECK_THROTTLE_ON_SCROLL).pipe(
            takeUntil(this._madeVisible),
            takeUntil(this._unsubscribeAll))
          .subscribe((event) => {
              this._checkVisible();
          });

        // check the visibility when media change events occur
        this._MediaWatcherService.onMediaChange$.pipe(
            debounceTime(VISIBILITY_CHECK_DEBOUNCE_ON_MEDIA_CHANGE),
            takeUntil(this._madeVisible),
            takeUntil(this._unsubscribeAll))
          .subscribe((event) => this._checkVisible());

        // when the item has been made visible, show the source image
        this._madeVisible
          .pipe(take(1))
          .subscribe(() => {

              this.actualHeight = null;
              this.image.nativeElement.src = this.item.images.fixed_width_downsampled.url;
              this._changeDetectorRef.detectChanges();
              this._madeVisible.complete();
          })

    }

    /**
     * After view init
     */
    ngAfterViewInit() {

        // used to display a proportioned skeleton
        this._setActualHeight();

        this._checkVisible();

        this._changeDetectorRef.detectChanges();

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    showDetail() {
        const dialogRef = this._dialog.open(ItemDetailComponent);
        
        dialogRef.componentInstance.item = this.item;

        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _selectRandomColor(): string {
        return SKELETON_COLOR_PALETTE[Math.floor(Math.random() * SKELETON_COLOR_PALETTE.length)];
    }

    private _setActualHeight() {

        this.actualHeight =
          (this.item.images.fixed_width_downsampled.height * this.image.nativeElement.width)
          / this.item.images.fixed_width_downsampled.width;

    }

    private _checkVisible() {

        if (this._isElementInViewport(this._ref.nativeElement, 1)) {
            this._madeVisible.next();
        }

    }

    /**
     * Ref. https://stackoverflow.com/questions/30943662/check-if-element-is-partially-in-viewport
     * @param el
     * @private
     */
    private _isElementInViewport (el, percentVisible) {

        let
          rect = el.getBoundingClientRect(),
          windowHeight = (window.innerHeight || document.documentElement.clientHeight);

        return !(
          Math.floor(100 - (((rect.top >= 0 ? 0 : rect.top) / +-rect.height) * 100)) < percentVisible ||
          Math.floor(100 - ((rect.bottom - windowHeight) / rect.height) * 100) < percentVisible
        )
    }
}
