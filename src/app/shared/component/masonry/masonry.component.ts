import { AfterViewInit, Component, Input, OnChanges, SimpleChanges, TemplateRef, ViewEncapsulation } from '@angular/core';

export interface MasonryItem<T> {
    data: T;
    weight: number;
}

@Component({
    selector     : 'app-masonry',
    templateUrl  : './masonry.component.html',
    styleUrls    : ['./masonry.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MasonryComponent implements OnChanges, AfterViewInit {
    @Input() columnsTemplate: TemplateRef<any>;
    @Input() columns: number;
    @Input() items: MasonryItem<any>[] | any[] = [];
    distributedColumns: any[] = [];

    /**
     * Constructor
     */
    constructor() {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On changes
     *
     * @param changes
     */
    ngOnChanges(changes: SimpleChanges): void {
        // Columns
        if ( 'columns' in changes )
        {
            // Distribute the items
            this._distributeItems();
        }

        // Items
        if ( 'items' in changes )
        {
            // Distribute the items
            this._distributeItems();
        }
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {
        // Distribute the items for the first time
        this._distributeItems();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Distribute items into columns
     */
    private _distributeItems(): void {

        // Return an empty array if there are no items
        if ( this.items.length === 0 )
        {
            this.distributedColumns = [];
            return;
        }

        // Prepare the distributed columns array
        this.distributedColumns = Array.from(Array(this.columns), item => ({items: [], weight: 0}));

        // Distribute the items to columns
        this.items.forEach((item) => {
            const column = this._selectColumn();
            column.items.push(item);
            column.weight = column.weight + item.weight;
        })
    }

    private _selectColumn(): any {
        let minColumn = this.distributedColumns[0];
        this.distributedColumns.forEach((column) => {
            minColumn = minColumn.weight > column.weight ? column : minColumn;
        });
        return minColumn;
    }
}
