import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: 'root'})
export class MediaWatcherService {

    private _onMediaChange: ReplaySubject<{ matchingAliases: string[]; }> = new ReplaySubject<{ matchingAliases: string[] }>(1);

    private displayNameMap = new Map([
        [Breakpoints.XSmall, 'xs'],
        [Breakpoints.Small, 'sm'],
        [Breakpoints.Medium, 'md'],
        [Breakpoints.Large, 'lg'],
        [Breakpoints.XLarge, 'xl'],
    ]);

  /**
     * Constructor
     */
    constructor(
        private _breakpointObserver: BreakpointObserver,
    ) {
        this._breakpointObserver.observe([
          Breakpoints.XSmall,
          Breakpoints.Small,
          Breakpoints.Medium,
          Breakpoints.Large,
          Breakpoints.XLarge,
        ]).pipe(
          map((state) => {

            // Prepare the observable values and set their defaults
            const matchingAliases: string[] = [];
            for (const query of Object.keys(state.breakpoints)) {
              if (state.breakpoints[query]) {
                matchingAliases.push(this.displayNameMap.get(query) ?? 'Unknown');
              }
            }

            // Execute the observable
            this._onMediaChange.next({
              matchingAliases
            });
          })
        ).subscribe();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for _onMediaChange
     */
    get onMediaChange$(): Observable<{ matchingAliases: string[] }> {
        return this._onMediaChange.asObservable();
    }

}
