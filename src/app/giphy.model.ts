export interface GiphyImageData
{
  height: number;
  width: number;
  size: number;
  url: string;
  mp4: string;
  mp4_size: number;
  webp: string;
  webp_size: number;
}
export interface GiphyImage
{
  id: string;
  type: 'gif' | undefined,
  url: string;
  slug: string;
  title: string;
  username: string;
  rating: string;
  import_datetime: Date;
  images: {
    fixed_width_downsampled: GiphyImageData,
    downsized: GiphyImageData,
  }
}

export interface GiphyPagination
{
  total_count: number;
  count: number;
  offset: number;
}

export interface GiphyMeta
{
  status: number;
  msg: string;
  response_id: string;
}

export interface GiphyResponse
{
  data: GiphyImage[];
  pagination: GiphyPagination;
  meta: GiphyMeta;
}
