import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { MediaWatcherService } from '../shared/services/media-watcher.service';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { MasonryItem } from '../shared/component/masonry/masonry.component';
import { GiphyFavoriteService } from '../giphy-favorite.service';
import { GiphyImage } from '../giphy.model';


@Component({
    selector       : 'app-favorites',
    templateUrl    : './favorites.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoritesComponent implements OnInit, OnDestroy {

    items: MasonryItem<GiphyImage>[];
    masonryColumns: number = 4;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _ref: ElementRef,
        private _changeDetectorRef: ChangeDetectorRef,
        private _MediaWatcherService: MediaWatcherService,
        private _scrollDispatcher: ScrollDispatcher,
        private _matDialog: MatDialog,
        private _giphyFavoriteService: GiphyFavoriteService,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._giphyFavoriteService.favorites$()
            .pipe(
              takeUntil(this._unsubscribeAll),
              filter((items) => !!items)
            )
            .subscribe((images) => {
                this.items = images.map((image) => ({ data: image, weight: +image.images.fixed_width_downsampled.height}));
                this._changeDetectorRef.detectChanges();
            });

        // Subscribe to media changes
        this._MediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({matchingAliases}) => {

                // Set the masonry columns
                //
                // This if block structured in a way so that only the
                // biggest matching alias will be used to set the column
                // count.
                if ( matchingAliases.includes('xl') )
                {
                    this.masonryColumns = 5;
                }
                else if ( matchingAliases.includes('lg') )
                {
                    this.masonryColumns = 4;
                }
                else if ( matchingAliases.includes('md') )
                {
                    this.masonryColumns = 3;
                }
                else if ( matchingAliases.includes('sm') )
                {
                    this.masonryColumns = 2;
                }
                else
                {
                    this.masonryColumns = 1;
                }

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Column track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFnColumn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Items track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFnItem(index, item: any): any {
        return item.data.id;
    }
}
