import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { FavoritesComponent } from './favorites/favorites.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from './shared/component/shared.module';


export const API_URL = 'https://api.giphy.com';
export const API_SEARCH_ENDPOINT = `${API_URL}/v1/gifs/search`
export const API_KEY = 'xxx';

export const SEARCH_DEFAULT_LIMIT = 25;
export const SEARCH_DEBOUNCE = 500;

export const VISIBILITY_CHECK_THROTTLE_ON_SCROLL = 500;
export const VISIBILITY_CHECK_DEBOUNCE_ON_MEDIA_CHANGE = 500;

export const SKELETON_COLOR_PALETTE = [
  'bg-gray-500',
  'bg-red-500',
  'bg-yellow-500',
  'bg-green-500',
  'bg-blue-500',
  'bg-indigo-500',
  'bg-purple-500',
  'bg-pink-500'
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FavoritesComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,

    SharedModule,

    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    ScrollingModule,
    MatToolbarModule,
    MatButtonModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
